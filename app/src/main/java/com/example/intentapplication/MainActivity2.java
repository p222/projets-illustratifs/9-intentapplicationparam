package com.example.intentapplication;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutactivite2);

        // Récupère le paramètre de l'intent via la clé utilisée
        String texte = getIntent().getStringExtra("letexte");

        // modifie le texte du label de l'activité avec cette valeur
        TextView textV = (TextView)findViewById(R.id.idTextView);
        textV.setText(texte);

    }
}